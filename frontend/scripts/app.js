

var getAccessToken = function () {
  var cookie = document.cookie;
  var params = document.cookie.split('&');
  for (var i = 0 ; i < params.length ; i += 1) {
    var pair = params[i].split('=')
    if(pair[0] === 'access_token')  {
      return pair[1];
    }
  }
  return null;
}

var access_token = getAccessToken();
if (access_token) {
  $.getJSON('https://api.github.com/user?access_token=' + access_token).done(function (user) {
    document.querySelector('#user-links a').innerHTML = '<img id="avatar" height="40" width="40" alt="avatar" src="' + user.avatar_url + '">'
  });
}

var editor = ace.edit('editor');
editor.setTheme('ace/theme/github');
editor.getSession().setMode('ace/mode/json');


var elFormat = document.querySelector('#menu .format');
var elCompact = document.querySelector('#menu .compact');

elFormat.addEventListener('click', function (e) {
  e.preventDefault()
  newvalue =  JSON.stringify(JSON.parse(editor.getValue()), null, "  ");
  editor.setValue(newvalue);
});

elCompact.addEventListener('click', function (e) {
  e.preventDefault()
  newvalue = JSON.stringify(JSON.parse(editor.getValue()));
  editor.setValue(newvalue);
});
    
var btnSubmit = document.querySelector('#send');
var form = document.jsonform;
form.addEventListener('submit', function (e) {
  e.preventDefault()
  $.post('/', {
    content: editor.getValue(),
    title: form.title.value
  }).done(function (res) {
    window.location.href = '/list';
  }).fail(function (e) {
    console.error(e);
    alert('something goes wrong');
  })
});
