###
http://json.chunghe.me/:id
###
express = require('express')
mongoose = require('mongoose')
path = require('path')
request = require('request')
hbs = require('hbs')

mongoose.connect('mongodb://localhost/jdata')

db = mongoose.connection
db.on('error', console.error.bind(console, 'connection error:'))

JsonDataSchema = new mongoose.Schema(
  user: {
    name: String,
    provider: String
  },
  title: String,
  content: {type: String, required: true}
)

UserDataSchema = new mongoose.Schema(
  name: {type: String, required: true}
  access_token: {type: String, required: true}
)


sendResponse = (res, code, results) ->
  if code is 200
    res.writeHead 200,
      "Content-Type": "application/json"
      "Access-Control-Allow-Origin": "*"
      "Access-Control-Allow-Headers": "X-Requested-With"
    res.end(JSON.stringify(results))
  else
    res.writeHead code


JsonData = mongoose.model('JsonData', JsonDataSchema)
UserData = mongoose.model('UserData', UserDataSchema)
publicFolder = path.resolve(__dirname + '/../frontend')

app = express()

app.use(express.static(publicFolder))
app.use(express.limit('5mb'))
app.use(express.bodyParser())
app.use(express.cookieParser())
app.set('view engine', 'htm')
app.engine('htm', hbs.__express)

# allow CORS with customize header 'X-Requested-With: XMLHttpRequest'
app.options('/list/page/:page', (req, res) ->
  res.header 'Access-Control-Allow-Origin', '*'
  res.header 'Access-Control-Allow-Credentials', true
  res.header 'Access-Control-Allow-Methods', 'GET, POST, OPTIONS, DELETE'
  res.header 'Access-Control-Allow-Headers', 'X-Requested-With'
  res.header 'Access-Control-Max-Age', '1728000'
  res.header 'Content-Length', '0'
  res.send(200)
)

app.get('/auth/github', (req, res) ->
  url = "https://github.com/login/oauth/authorize?client_id=d701bea4cd70a7d937d8&scope=user,user:email&redirect_uri=http://json.chunghe.me/auth/github/callback"
  res.redirect(url)
)

app.get('/auth/github/callback', (req, res) ->
  code = req.query.code
  u = "https://github.com/login/oauth/access_token?client_id=d701bea4cd70a7d937d8&client_secret=d47e03e8034d4c04ca9fb3501be79ff4174d8caf&code=#{code}"
  request.get({url:u, json: true},  (err, rsp, body) ->
    if err
      cosnole.log 'error', err
    else
      access_token = body.access_token
      res.cookie('access_token', access_token)
      res.redirect('/')
  )
)

app.get('/signout', (req, res) ->
  res.clearCookie('access_token')
  res.redirect('/')
)

getUserByAccessToken = (access_token, callback) ->
  if not access_token
    callback(null)
    return false

  options =
    url: "https://api.github.com/user?access_token=#{access_token}"
    json: true
    headers: {
      'User-Agent': 'chunghe'
    }
  UserData.findOne(access_token: access_token, (err, doc) ->
    if (doc)
      callback(doc.name)
    else
      request(options, (err, rsp, user) ->
        userData = new UserData(access_token: access_token, name: user.login)
        userData.save ((err, doc) ->
          console.log 'err', err if (err)
        )
        callback(user.login)
      )
  )

app.get('/list/page/:page', (req, res) ->
  perPage = 10
  page = +req.params.page

  access_token = req.cookies.access_token

  getUserByAccessToken(access_token, (name) ->
    if (access_token)
      condition = $or: [{'user.name': name}, {user: null}]
    else
      condition = $or: [{user: null}]

    console.log 'condition', condition
    JsonData.find(
      condition
    ).select('_id user title').sort(_id: -1).exec((err, docs) ->
      count = docs.length
      res.header 'Access-Control-Allow-Origin', '*'
      res.set('Content-Type', 'application/json; charset=utf-8')
      res.send(docs: docs.slice(perPage * page, perPage * (page + 1)), page:page, pages: Math.ceil(count/perPage))
    )
  )
)

app.get('/list', (req, res) ->
  res.sendfile(publicFolder + '/list.htm')
)

app.delete('/delete/:id', (req, res) ->
  id = req.params.id
  JsonData.remove(_id: req.params.id, (err) ->
    if err
      res.send(500, err.message)
    else
      res.send(200, {id: id})
  )
)

app.options('/:id', (req, res) ->
  console.log 'req.heaers', req.headers
  res.header 'Access-Control-Allow-Origin', '*'
  res.header 'Access-Control-Allow-Credentials', true
  res.header 'Access-Control-Allow-Methods', 'GET, POST, OPTIONS, DELETE'
  res.header 'Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, session'
  res.header 'Access-Control-Max-Age', '1728000'
  res.header 'Content-Length', '0'
  res.send(200)
)

handleRequest = (req, res) ->
  return false if req.url is '/favicon.ico'
  id = req.params.id
  t = req.query.t
  JsonData.findOne(_id: id, (err, doc) ->
    if err
      res.send(500, err.message)
      console.log 'error', err
    else if doc is null
      res.send(404, 'Not Found')
    else
      res.set('Access-Control-Allow-Origin', '*')
      res.set('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
      res.set('Content-Type', 'application/json; charset=utf-8')
      if t is undefined
        res.send(doc.content)
      else
        setTimeout (->
          res.send(doc.content)
        ), 1000 * t
  )

app.get('/:id', handleRequest)
app.post('/:id', handleRequest)

app.get('/', (req, res) ->
  access_token = req.cookies.access_token
  u = "https://api.github.com/user?access_token=#{access_token}"
  request.get({url: u, json: true}, (err, rsp, user) ->
    res.render(publicFolder + '/index.htm', {'loggedIn': !!access_token, user: user})
  )
)

saveToDb = (tosave, res)->
  jsonData = new JsonData(tosave)
  jsonData.save((err, doc) ->
    if err
      res.send(500, 'error while saving')
    else
      res.send(id: doc.id)
  )



app.post('/', (req, res) ->
  access_token = req.cookies.access_token
  tosave =
    title: req.body.title
    content: req.body.content
  if access_token
    getUserByAccessToken(access_token, (name) ->
      tosave.user = name: name, provider: 'github'
      saveToDb(tosave, res)
    )
  else
    saveToDb(tosave, res)
)

app.listen(3002, 'localhost')

